package com.example.root.khozeeradio;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * Created by root on 11/16/17.
 */

public class NewTrendingRecyclerAdapter extends RecyclerView.Adapter<NewTrendingRecyclerAdapter.MyViewHolder> {
    Context ctx;
    ArrayList<SongDTO> songList;

    public NewTrendingRecyclerAdapter(Context ctx, ArrayList<SongDTO> songList) {
        this.ctx = ctx;
        this.songList = songList;
    }

    @Override
    public NewTrendingRecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.new_trending_recycler_list_inside,parent,false);
        MyViewHolder holder=new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(NewTrendingRecyclerAdapter.MyViewHolder holder, int position) {
        Glide.with(ctx).load(songList.get(position).getImage()).into(holder.songImage);
        holder.songTitle.setText(songList.get(position).getSongTitle());
    }

    @Override
    public int getItemCount() {
        return songList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView songImage;
        TextView songTitle;
        public MyViewHolder(View itemView) {
            super(itemView);
            songImage=(ImageView)itemView.findViewById(R.id.song_image_view);
            songTitle=(TextView)itemView.findViewById(R.id.song_title);
        }
    }
}

package com.example.root.khozeeradio;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by root on 11/16/17.
 */

public class PradeshRecyclerAdapter extends RecyclerView.Adapter<PradeshRecyclerAdapter.MyViewHolder> {
    ArrayList<PradeshDTO> pradeshList;
    Context ctx;

    public PradeshRecyclerAdapter(ArrayList<PradeshDTO> pradeshList, Context ctx) {
        this.pradeshList = pradeshList;
        this.ctx = ctx;
    }

    @Override
    public PradeshRecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.new_trending_recycler_list_inside,parent,false);
        MyViewHolder holder=new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(PradeshRecyclerAdapter.MyViewHolder holder, int position) {
        Glide.with(ctx).load(pradeshList.get(position).getImage()).into(holder.pradeshImage);
        holder.pradeshName.setText(pradeshList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return pradeshList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView pradeshImage;
        TextView pradeshName;
        public MyViewHolder(View itemView) {
            super(itemView);
            pradeshImage=(ImageView)itemView.findViewById(R.id.song_image_view);
            pradeshName=(TextView)itemView.findViewById(R.id.song_title);
        }
    }
}

package com.example.root.khozeeradio;

/**
 * Created by root on 11/16/17.
 */

public class PradeshDTO {
    private int id,image;
    private String name;

    //delete later
    public PradeshDTO(int image, String name) {
        this.image = image;
        this.name = name;
    }

    public PradeshDTO(int id, int image, String name) {
        this.id = id;
        this.image = image;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

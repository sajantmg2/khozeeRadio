package com.example.root.khozeeradio;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class MainActivityDashboard extends AppCompatActivity {
    ArrayList<SongDTO> songsList;
    ArrayList<PradeshDTO> pradeshList;
    private TextView newReleased;
    RecyclerView newTrendingRecyclerView,pradeshRecyclerView;
    int[] songsImage=new int[]{
            R.drawable.rustom,
            R.drawable.fitoor,
            R.drawable.badlapur,
            R.drawable.premgeet2,
    };

    String[] songTitle={"Rustom","Fitoorrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrm","Jeena Jeena","Motorcyclema"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dashboard);

        newReleased=(TextView)findViewById(R.id.new_release);
        newReleased.setText("New Release & Trending");

        newTrendingRecyclerView=(RecyclerView)findViewById(R.id.new_trending_recycler_view);
        newTrendingRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false));
        songsList=new ArrayList<>();

        for (int i=0;i<songTitle.length;i++){
            SongDTO songs=new SongDTO(songsImage[i],songTitle[i]);
            songsList.add(songs);
        }
        NewTrendingRecyclerAdapter adapter=new NewTrendingRecyclerAdapter(getApplicationContext(),songsList);
        newTrendingRecyclerView.setAdapter(adapter);

        setPradeshRecyclerView();
    }

    private void setPradeshRecyclerView() {
        pradeshList=new ArrayList<>();
        int[] pradeshImage=new int[]{
                R.drawable.rustom,
                R.drawable.fitoor,
                R.drawable.badlapur,
                R.drawable.premgeet2,
                R.drawable.fitoor,
                R.drawable.badlapur,
                R.drawable.premgeet2
        };

        String[] pradeshName={"Pradesh 1","Pradesh 2","Pradesh 3","Pradesh 4","Pradesh 5","Pradesh 6","Pradesh 7"};

        pradeshRecyclerView=(RecyclerView)findViewById(R.id.pradesh_recycler_view);
        pradeshRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false));
        for (int i=0;i<pradeshName.length;i++){
            PradeshDTO pradeshs=new PradeshDTO(pradeshImage[i],pradeshName[i]);
            pradeshList.add(pradeshs);
            System.out.println(pradeshName[i]);
        }
        PradeshRecyclerAdapter adapter1=new PradeshRecyclerAdapter(pradeshList,getApplicationContext());
        pradeshRecyclerView.setAdapter(adapter1);
    }

}

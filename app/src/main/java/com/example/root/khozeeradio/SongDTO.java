package com.example.root.khozeeradio;

/**
 * Created by root on 11/16/17.
 */

public class SongDTO {
    private int id;
    private int image;
    private String ImageUrl,songTitle,songAlbum,singerName;

    public SongDTO() {
    }

    public SongDTO(int image, String songTitle) {
        this.image = image;
        this.songTitle = songTitle;
    }

    public SongDTO(int id, int image, String imageUrl, String songTitle, String songAlbum, String singerName) {
        this.id = id;
        this.image = image;
        ImageUrl = imageUrl;
        this.songTitle = songTitle;
        this.songAlbum = songAlbum;
        this.singerName = singerName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getSongTitle() {
        return songTitle;
    }

    public void setSongTitle(String songTitle) {
        this.songTitle = songTitle;
    }

    public String getSongAlbum() {
        return songAlbum;
    }

    public void setSongAlbum(String songAlbum) {
        this.songAlbum = songAlbum;
    }

    public String getSingerName() {
        return singerName;
    }

    public void setSingerName(String singerName) {
        this.singerName = singerName;
    }
}
